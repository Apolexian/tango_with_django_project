Tango With Django WAD2 Project
-------------------------------

Repository for the Rango project from the Tango With Django Book (https://www.tangowithdjango.com/) for my university Web App Development 2 course (https://www.gla.ac.uk/coursecatalogue/course/?code=COMPSCI2021). In particular this will focus on the first 10 chapters of the book (**v2020-01a**).

## Technologies
Python **v3.7.2** <br>
Django **v2.2.3** (Note that this version is not secure and 2.2.9 or later should be used. This version was used for purpose of the course due to given specification).<br>
Pillow **v7.0.0**

## Setup

Create workspace directory (e.g ~/workdir) and change into it:
```
$ mkdir ~/workdir
$ cd ~/workdir
```

Create virtual environment (using virtualenvwrapper, for anaconda see conda docs):
```
$ mkvirtualenv rango --no-site-packages
$ workon rango
```
Clone repo:
```
(rango)$ git clone git@github.com:Apolexian/tango_with_django_project.git
```
Change into created directory and run pip_install script: <br>
```
(rango)$ cd tango-with-django-project
(rango)$ ./pip_install
```
Start local server: 
```
(rango)$ python manage.py runserver
```

Open browser and go to  http://127.0.0.1:8000/rango <br>
You should now see the Rango project webpage.